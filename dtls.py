#!/usr/bin/python3
from tabulate import tabulate
import subprocess, os, glob, hashlib

status_file = '/var/lib/dpkg/status'
lists_directory = '/var/lib/apt/lists/'

def package_md5sum(deb):
    """ This function simply returns a string containing the md5sum of a given package"""
    hash_md5 = hashlib.md5()
    with open(deb, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
        return hash_md5.hexdigest()

def load_list_files():
    # cd into the lists folder (this requires root)
    os.chdir(lists_directory)
    big_list = []
    # only use our lists
    for lists in glob.glob('*repo.dragontailsystems.com*'):
        with open(lists, "rb") as l:
            for line in l:
                big_list.append(line)
    # this returns all the lists concatenated together
    return big_list


def load_statusfile():
    """This function iterates over the package_list list and looks for
    matching Debian packages in the apt cache. We run dpkg-deb --show on all packages and build another list in order to match it with packages_list"""
    status = []
    # open the status file buffered and readonly
    with open(status_file, "rb") as f:
        for line in f:
            # encode the byte sequence into a string
            line = line.decode("utf-8")
            # add it to the status list
            status.append(line)
        return status

def load_status_entry(status, package_name):
    """ Using the status file we will pull the metadata of the provided package_name """
    metapackage = []
    line_to_find = "Package: " + package_name + '\n'
    while True:
        for line in status:
            if line_to_find in line:
                _temp_index_start = status.index(line)
                temp_status = status[_temp_index_start:]
        for line in temp_status:
            metapackage.append(line)
            if line in '\n':
                return metapackage

def create_meta_structure(status_entry):
    """ This function takes the list created by load_status_entry() and formats it into a more manageable structure """
    # this lambda functions allow for quick and easy functions, so I can define a way to split a string and just call it later
    format = lambda a : a.split(": ", 1)
    format_depends = lambda a : a.split(", ")
    format_version = lambda a : a.split(" ", 1)
    structure = []
    depends = []
    for line in status_entry:
        # this removes the newline character from the entries in the list
        _ = line.rstrip('\n')
        structure.append(_)
        # these build our return variables for use in the metapackage class
    for line in structure:
        if 'Package' in line:
            package = format(line)
            structure.remove(line)
        if 'Version' in line:
            version = format(line)
            structure.remove(line)
        if 'Depends' in line:
            _depends = format(line)
            structure.remove(line)
            # because depends is a list containig more lists, we need to iterate over it
            dependancys = format_depends(_depends[1])
            for entries in dependancys:
                _ = format_version(entries)
                depends.append(_)
    return package,version,depends

class metapackage:
    def __init__(self, package, version, depends):
        _depends = []
        self.package = package[1]
        self.version = version[1]
        for dependancy in depends:
            # if there is a version number, strip off the junk
            if len(dependancy) is 2:
                dependancy = [dependancy[0],dependancy[1][3:-1]]
                _depends.append(dependancy)
            else:
                _depends.append([dependancy[0],'Any'])
        self.depends = _depends

def build_table():
    # load our status file
    list = load_statusfile()

    # search and use for the dtrelease package
    metainfo = load_status_entry(list, 'dtrelease')

    # pull the data from the status file into the below 3 variables
    dt_package, dt_version, dt_depends = create_meta_structure(metainfo)

    # create a metapackage object
    dtrelease = metapackage(dt_package, dt_version, dt_depends)

    dt_table = []
    dt_table.append([dtrelease.package,dtrelease.version,dtrelease.version])
    for dependancy in dtrelease.depends:
        _status = load_status_entry(list, dependancy[0])
        _status_entry = create_meta_structure(_status)
        _package = metapackage(*_status_entry)
        dt_table.append([dependancy[0], dependancy[1], _package.version])

    # set our table headers
    headers=['package', 'provided by ' + dtrelease.version, 'currently installed']
    # sort the list based on the 0th element
    dt_table.sort(key=lambda x: x[0])
    print(tabulate(dt_table, headers, tablefmt='fancy_grid'))

def main():
    build_table()

main()

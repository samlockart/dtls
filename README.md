# dtls.py - a dragontail software listing tool

dtls.py serves to provide an easy way to query which Dragontail programs are meant to be installed (through the metapackage) and which packages are actually installed.
